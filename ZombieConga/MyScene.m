//
//  MyScene.m
//  ZombieConga
//
//  Created by student on 16/10/2014.
//  Copyright (c) 2014 chester. All rights reserved.
//

#import "MyScene.h"
#import "GameOverScene.h"
@import AVFoundation;


static const float ZOMBIE_MOVE_POINTS_PER_SEC = 120.0;
static const float ZOMBIE_ROTATE_RADIANS_PER_SEC = 4 * M_PI;
static const float CAT_MOVE_POINTS_PER_SEC = 120.0;
#define ARC4RANDOM_MAX 0x100000000
static const float BG_POINTS_PER_SEC = 50;

int _lives;
BOOL _gameOver;
AVAudioPlayer *_backgroundMusicPlayer;
SKNode *_bgLayer;

static inline CGFloat ScalarRandomRange(CGFloat min,
                                        CGFloat max)
{
    return floorf(((double)arc4random() / ARC4RANDOM_MAX) * (max-min)+ min);
}

static inline CGPoint CGPointAdd(const CGPoint a, const CGPoint b)
{
    return CGPointMake(a.x + b.x, a.y + b.y);
}
static inline CGPoint CGPointSubstract(const CGPoint a, const CGPoint b)
{
    return CGPointMake(a.x - b.x, a.y - b.y);
}
static inline CGPoint CGPointMultiplyScalar(const CGPoint a, const CGFloat b)
{
    return CGPointMake(a.x * b, a.y * b);
}
static inline CGFloat CGPointLength(const CGPoint a)
{
    return sqrtf(a.x * a.x + a.y * a.y);
}
static inline CGPoint CGPointNormalise(const CGPoint a)
{
    CGFloat length = CGPointLength(a);
    return CGPointMake(a.x / length, a.y / length);
}
static inline CGFloat CGPointToAngle(const CGPoint a)
{
    return atan2f(a.y, a.x);
}
static inline CGFloat ScalarSign(CGFloat a)
{
    return a >= 0 ? 1 : -1;
}
static inline CGFloat ScalarShortestAngleBetween(const CGFloat a, const CGFloat b)
{
    CGFloat difference = b-a;
    CGFloat angle = fmodf(difference, M_PI * 2);
    if (angle >= M_PI) {
        angle -= M_PI * 2;
    }else if (angle <= -M_PI){
        angle += M_PI * 2;
    }
    return angle;
}


@implementation MyScene
{
    SKSpriteNode *_zombie;
    NSTimeInterval _lastUpdateTime;
    NSTimeInterval _dt;
    CGPoint _velocity;
    CGPoint _lastTouchLocation;
    SKAction *_zombieAnimation;
    SKAction *_catCollisionSound;
    SKAction *_enemyCollisionSound;
    SKAction *_blinkAction;
    SKAction *_resetZombie;
    SKAction *_turnMeGreen;
    BOOL _zombieInvincible;
    CGPoint _newCatInCongaLinePosition;
}

-(id)initWithSize:(CGSize)size
{
    if (self = [super initWithSize:size]){
        
        _bgLayer = [SKNode node];
        [self addChild:_bgLayer];
        
        //set background color
        self.backgroundColor = [SKColor whiteColor];
        
        //play background music
        [self playBackgroundMusic:@"bgMusic.mp3"];
        
        //game properties
        _lives = 5;
        _gameOver = NO;
        
        
        
        //create bg spritenode
        
        for (int i = 0; i < 2; i++) {
            SKSpriteNode *bg = [SKSpriteNode spriteNodeWithImageNamed:@"background"];
            //practice
            //bg.position = CGPointMake(self.size.width/2, self.size.height/2);
            bg.anchorPoint = CGPointZero;
            bg.position = CGPointMake(i * bg.size.width, 0);
            //practice
            //bg.zRotation = M_PI / 8;
            
            bg.name = @"bg";
            
            //add bg to the scene
            [_bgLayer addChild:bg];
        }
        
            //practice
            //CGSize mySize = bg.size;
            //NSLog(@"Size: %@", NSStringFromCGSize(mySize));
        
        //create zombie spritenode
        _zombie = [SKSpriteNode spriteNodeWithImageNamed:@"zombie1"];
        //position the zombie
        _zombie.position = CGPointMake(100, 100);
        _zombie.zPosition = 100;
        
            //practice - double the size of the zombie
            
            //CGSize newZombieSize = _zombie.size;
            //newZombieSize.height *= 2;
            //newZombieSize.width *= 2;
            //_zombie.size = newZombieSize;
            
            //using built in method method - scale
            //[_zombie setScale:2.0];
        
        //add the zombie to the scene
        [_bgLayer addChild:_zombie];
        _zombieInvincible = FALSE;
        
        NSMutableArray *textures = [NSMutableArray arrayWithCapacity:10];
        for (int i = 1; i < 4; i++) {
            NSString *textureName = [NSString stringWithFormat:@"zombie%d", i];
            SKTexture *texture = [SKTexture textureWithImageNamed:textureName];
            [textures addObject:texture];
        }
        
        for (int i = 4; i > 1; i--) {
            NSString *textureName = [NSString stringWithFormat:@"zombie%d", i];
            SKTexture *texture = [SKTexture textureWithImageNamed:textureName];
            [textures addObject:texture];
        }
        _zombieAnimation = [SKAction animateWithTextures:textures timePerFrame:0.1];
        
        float blinkTimes = 10;
        float blinkDuration = 3.0;
        _blinkAction =
        [SKAction customActionWithDuration:blinkDuration actionBlock:^(SKNode *node, CGFloat elapsedTime) {
            float slice = blinkDuration / blinkTimes;
            float reminder = fmodf(elapsedTime, slice);
            node.hidden = reminder > slice /2;
        }];
        _turnMeGreen =
        [SKAction colorizeWithColor:[SKColor greenColor] colorBlendFactor:1.0 duration:0.2];
        
        _resetZombie = [SKAction customActionWithDuration:0.1 actionBlock:^(SKNode *node, CGFloat elapsedTime) {
            _zombie.hidden = NO;
            _zombieInvincible = FALSE;
        }];
        
        [self runAction:[SKAction repeatActionForever:
                        [SKAction sequence:@[
                              [SKAction performSelector:@selector(spawnEnemy) onTarget:self],
                              [SKAction waitForDuration:2.0]]
                         ]]];
        [self runAction:[SKAction repeatActionForever:
                         [SKAction sequence:@[
                                              [SKAction performSelector:@selector(spawnCat) onTarget:self],
                                              [SKAction waitForDuration:1.0]
                                              ]]]];
        _catCollisionSound = [SKAction playSoundFileNamed:@"hitCat.wav" waitForCompletion:NO];
        _enemyCollisionSound = [SKAction playSoundFileNamed:@"hitCatLady.wav" waitForCompletion:NO];
        
    }
    return self;
    
}

- (void)update:(NSTimeInterval)currentTime
{
    if (_lastUpdateTime) {
        _dt = currentTime - _lastUpdateTime;
    }else {
        _dt = 0;
    }
    _lastUpdateTime = currentTime;
    //NSLog(@"%0.2f ms since last update", _dt * 1000);
    
    /*
    if (CGPointLength(_velocity) > 0){
        if ([self keepMoving:_lastTouchLocation]){
            [self moveSprite:_zombie velocity:_velocity];
            [self boundsCheckPlayer];
            [self rotateSprite:_zombie toFace: _velocity rotateRadiansPerSec:ZOMBIE_ROTATE_RADIANS_PER_SEC];
        }else{
            _zombie.position = _lastTouchLocation;
            _velocity = CGPointZero;
            [self stopZombieAnimation];
        }
    }
    */
    
    [self moveSprite:_zombie velocity:_velocity];
    [self boundsCheckPlayer];
    [self rotateSprite:_zombie toFace: _velocity rotateRadiansPerSec:ZOMBIE_ROTATE_RADIANS_PER_SEC];
    
    [self moveTrain];
    [self moveBg];
    //[self checkCollisions];
    if (_lives <= 0 && !_gameOver) {
        _gameOver = YES;
        NSLog(@"Too bad...");
        [_backgroundMusicPlayer stop];
        SKScene *gameOverScene = [[GameOverScene alloc] initWithSize:self.size won:NO];
        SKTransition *reveal = [SKTransition flipHorizontalWithDuration:0.5];
        [self.view presentScene:gameOverScene transition:reveal];
    }
    
}

- (void)didEvaluateActions
{
    [self checkCollisions];
    if (_zombieInvincible) {
        if (![_zombie actionForKey:@"blinking"]){
            [_zombie runAction:[SKAction sequence:@[_blinkAction, _resetZombie]] withKey:@"blinking"];
        }
    }
}

- (void)moveSprite:(SKSpriteNode *)sprite
          velocity:(CGPoint)velocity
{
    CGPoint amountToMove = CGPointMultiplyScalar(velocity, _dt);
    //NSLog(@"Amount to move: %@", NSStringFromCGPoint(amountToMove));
    sprite.position = CGPointAdd(_zombie.position, amountToMove);
}

-(void)rotateSprite:(SKSpriteNode *)sprite
             toFace:(CGPoint)velocity
rotateRadiansPerSec:(CGFloat)rotateRadiansPerSec
{
    //sprite.zRotation = CGPointToAngle(direction);
    float turnAngle = CGPointToAngle(velocity);
    float shortest = ScalarShortestAngleBetween(sprite.zRotation, turnAngle);
    float amtToRotate = rotateRadiansPerSec * _dt;
    //NSLog(@"turning to: %f shortest: %f amtToRotate: %f", turnAngle, shortest, amtToRotate);
    if (ABS(shortest) < amtToRotate) {
        amtToRotate = ABS(shortest);
        //NSLog(@"Using shortest!");
    }
    if (ScalarSign(shortest) == 1){
        //NSLog(@"Turning left/Not turning.");
        sprite.zRotation += amtToRotate;
    }else{
        sprite.zRotation -= amtToRotate;
        //NSLog(@"Turning right.");
    }
}

-(void)moveZombieToward:(CGPoint)location
{
    [self startZombieAnimation];
    CGPoint offset = CGPointSubstract(location, _zombie.position);
    //CGFloat length = CGPointLength(offset);
    CGPoint direction = CGPointNormalise(offset);
    _velocity = CGPointMultiplyScalar(direction, ZOMBIE_MOVE_POINTS_PER_SEC);
}

- (BOOL)keepMoving:(CGPoint)endPosition
{
    CGFloat length = CGPointLength(CGPointSubstract(_zombie.position, _lastTouchLocation));
    CGFloat nextStepLength = ZOMBIE_MOVE_POINTS_PER_SEC * _dt;
    if (length <= nextStepLength){
        return FALSE;
    }else {
        return TRUE;
    }
}

//touch handlers
- (void)touchesBegan:(NSSet *)touches
           withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint touchLocation = [touch locationInNode:_bgLayer];
    _lastTouchLocation = touchLocation;
    [self moveZombieToward:touchLocation];
}

- (void)touchesMoved:(NSSet *)touches
           withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint touchLocation = [touch locationInNode:_bgLayer];
    _lastTouchLocation = touchLocation;
    [self moveZombieToward:touchLocation];
}

- (void)touchesEnded:(NSSet *)touches
           withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint touchLocation = [touch locationInNode:_bgLayer];
    _lastTouchLocation = touchLocation;
    [self moveZombieToward:touchLocation];
}

//bounds check
- (void)boundsCheckPlayer
{
    CGPoint newPosition = _zombie.position;
    CGPoint newVelocity = _velocity;
    
    CGPoint bottomLeft = [_bgLayer convertPoint:CGPointZero fromNode:self];
    CGPoint topRight = [_bgLayer convertPoint:CGPointMake(self.size.width, self.size.height) fromNode:self  ];
    
    if (newPosition.x <= bottomLeft.x) {
        newPosition.x = bottomLeft.x;
        newVelocity.x = -newVelocity.x;
    }
    if (newPosition.x >= topRight.x) {
        newPosition.x = topRight.x;
        newVelocity.x = -newVelocity.x;
    }
    if (newPosition.y <= bottomLeft.y) {
        newPosition.y = bottomLeft.y;
        newVelocity.y = -newVelocity.y;
    }
    if (newPosition.y >= topRight.y) {
        newPosition.y = topRight.y;
        newVelocity.y = -newVelocity.y;
    }
    _zombie.position = newPosition;
    _velocity = newVelocity;
}

-(void)spawnEnemy
{
    SKSpriteNode *enemy = [SKSpriteNode spriteNodeWithImageNamed:@"enemy"];
    enemy.name = @"enemy";
    CGPoint enemyScenePos = CGPointMake(
                                        self.size.width + enemy.size.width/2,
                                        ScalarRandomRange(enemy.size.height/2,
                                                          self.size.height-enemy.size.height/2));
    enemy.position = [_bgLayer convertPoint:enemyScenePos fromNode:self];
    
    [_bgLayer addChild:enemy];
    
    SKAction *actionMove = [SKAction moveTo:CGPointMake(enemy.position.x - self.size.width, enemy.position.y) duration:2.0];
    SKAction *actionRemove = [SKAction removeFromParent];
    [enemy runAction:[SKAction sequence:@[actionMove, actionRemove]]];
    
    NSLog(@"enemy x: %f y: %f", enemy.position.x, enemy.position.y);
    NSLog(@"bglayer x: %f y: %f", _bgLayer.position.x, _bgLayer.position.y);
    NSLog(@"self width: %f height: %f", self.size.width, self.size.height);
    
    
    /* removed for now
    SKAction *actionMidMove = [SKAction moveByX:-self.size.width/2-enemy.size.width/2
                                              y:-self.size.height/2+enemy.size.height/2
                                       duration:1.0];
    SKAction *actionMove =[SKAction moveByX:-self.size.width/2-enemy.size.width/2
                                          y:self.size.height/2+enemy.size.height/2
                                   duration:1.0];
    SKAction *wait = [SKAction waitForDuration:1.0];
    SKAction *logMessage = [SKAction runBlock:^{
        NSLog(@"Reached bottom.");
    }];
    //SKAction *reverseMid = [actionMidMove reversedAction];
    //SKAction *reverseMove = [actionMove reversedAction];
    SKAction *sequence =
    [SKAction sequence:@[actionMidMove, logMessage, wait, actionMove]];
    sequence = [SKAction sequence:@[sequence, [sequence reversedAction]]];
    SKAction *repeat = [SKAction repeatActionForever:sequence];
    [enemy runAction:repeat];
     */
}

- (void)startZombieAnimation
{
    if (![_zombie actionForKey:@"animation"]) {
        [_zombie runAction:
         [SKAction repeatActionForever:_zombieAnimation]
                   withKey:@"animation"];
    }
}
- (void)stopZombieAnimation
{
    [_zombie removeActionForKey:@"animation"];
}


-(void)spawnCat
{
    SKSpriteNode *cat = [SKSpriteNode spriteNodeWithImageNamed:@"cat"];
    cat.name = @"cat";
    CGPoint catScenePos = CGPointMake(
                                      ScalarRandomRange(0, self.size.width),
                                      ScalarRandomRange(0, self.size.height));
    
    cat.position = [_bgLayer convertPoint:catScenePos fromNode:self];
    
    cat.xScale = 0;
    cat.yScale = 0;
    
    [_bgLayer addChild:cat];
    //actions
    cat.zRotation = -M_PI / 16;
    SKAction *appear = [SKAction scaleTo:1.0 duration:0.5];
    SKAction *leftWiggle = [SKAction rotateByAngle:M_PI /8 duration:0.5];
    SKAction *rightWiggle = [leftWiggle reversedAction];
    SKAction *fullWiggle = [SKAction sequence:@[leftWiggle,
                                                rightWiggle]];
    //SKAction *wiggleWait = [SKAction repeatAction:fullWiggle count:10];
    //SKAction *wait = [SKAction waitForDuration:10.0];
    SKAction *scaleUp = [SKAction scaleBy:1.2 duration:0.25];
    SKAction *scaleDown = [scaleUp reversedAction];
    SKAction *fullScale = [SKAction sequence:@[scaleUp, scaleDown, scaleUp, scaleDown]];
    SKAction *group = [SKAction group:@[fullScale, fullWiggle]];
    SKAction *groupWait = [SKAction repeatAction:group count:10];
    SKAction *disappear = [SKAction scaleTo:0.0 duration:0.5];
    SKAction *removeFromparent = [SKAction removeFromParent];
    [cat runAction:
     [SKAction sequence:@[appear, groupWait, disappear, removeFromparent]]];
}

-(void)checkCollisions
{
    [_bgLayer enumerateChildNodesWithName:@"cat" usingBlock:^(SKNode *node, BOOL *stop) {
        SKSpriteNode *cat = (SKSpriteNode *)node;
        if (CGRectIntersectsRect(cat.frame, _zombie.frame)) {
            //[cat removeFromParent];
            [cat removeAllActions];
            cat.name = @"train";
            cat.xScale = 1.0;
            cat.yScale = 1.0;
            cat.zRotation = 0;
            [self runAction:_catCollisionSound];
            [cat runAction:_turnMeGreen];
            //_newCatInCongaLinePosition = cat.position;
        }
    }];
    if (_zombieInvincible == FALSE) {
        [_bgLayer enumerateChildNodesWithName:@"enemy" usingBlock:^(SKNode *node, BOOL *stop) {
            SKSpriteNode *enemy = (SKSpriteNode *)node;
            CGRect smallerFrame = CGRectInset(enemy.frame, 20, 20);
            if (CGRectIntersectsRect(smallerFrame, _zombie.frame)) {
                //[enemy removeFromParent];
                [self loseCats];
                _lives--;
                _zombieInvincible = TRUE;
                [self runAction: _enemyCollisionSound];
                
            }
        }];
    }
}

-(void)moveTrain
{
    __block int trainCount = 0;
    __block CGPoint targetPosition = _zombie.position;
    [_bgLayer enumerateChildNodesWithName:@"train" usingBlock:^(SKNode *node, BOOL *stop) {
        trainCount++;
        if (!node.hasActions) {
            float actionDuration = 0.3;
            CGPoint offset = CGPointSubstract(targetPosition, node.position);
            CGPoint direction = CGPointNormalise(offset);
            CGPoint amountToMovePerSec = CGPointMultiplyScalar(direction, CAT_MOVE_POINTS_PER_SEC);
            CGPoint amountToMove = CGPointMultiplyScalar(amountToMovePerSec, actionDuration);
            SKAction *moveAction = [SKAction moveByX:amountToMove.x
                                                   y:amountToMove.y
                                            duration:actionDuration];
            [node runAction:moveAction];
        }
        targetPosition = node.position;
    }];
    if (trainCount >= 30 && !_gameOver) {
        _gameOver = YES;
        NSLog(@"I win!");
        [_backgroundMusicPlayer stop];
        SKScene *gameOverScene = [[GameOverScene alloc]initWithSize:self.size won:YES];
        SKTransition *reveal = [SKTransition flipHorizontalWithDuration:0.5];
        [self.view presentScene:gameOverScene transition:reveal];
    }
}

-(void)loseCats
{
    //1
    __block int loseCount = 0;
    [_bgLayer enumerateChildNodesWithName:@"train" usingBlock:^(SKNode *node, BOOL *stop) {
        //2
        CGPoint randomSpot = node.position;
        randomSpot.x = ScalarRandomRange(-100, 100);
        randomSpot.y = ScalarRandomRange(-100, 100);
        
        //3
        node.name = @"";
        [node runAction:
         [SKAction sequence:@[
                [SKAction group:@[
                    [SKAction rotateByAngle:M_PI * 4 duration:1.0],
                    [SKAction moveTo:randomSpot duration:1.0],
                    [SKAction scaleTo:0 duration:1.0]
                ]],
                [SKAction removeFromParent]
                ]]];
        
        loseCount++;
        if (loseCount >= 2) {
            *stop = YES;
        }
    }];
}

-(void)playBackgroundMusic:(NSString *)filename
{
    NSError *error;
    NSURL *backgroundMusicURL = [[NSBundle mainBundle] URLForResource:filename withExtension:nil];
    _backgroundMusicPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:backgroundMusicURL error:&error];
    _backgroundMusicPlayer.numberOfLoops = -1;
    [_backgroundMusicPlayer prepareToPlay];
    [_backgroundMusicPlayer play];
    
}

-(void)moveBg
{
    CGPoint bgVelocity = CGPointMake(-BG_POINTS_PER_SEC, 0);
    CGPoint amtToMove = CGPointMultiplyScalar(bgVelocity, _dt);
    _bgLayer.position = CGPointAdd(_bgLayer.position, amtToMove);
    
    [_bgLayer enumerateChildNodesWithName:@"bg" usingBlock:^(SKNode *node, BOOL *stop) {
        SKSpriteNode *bg = (SKSpriteNode *)node;
        CGPoint bgScreenPos = [_bgLayer convertPoint:bg.position toNode:self];
        if (bgScreenPos.x <= -bg.size.width) {
            bg.position = CGPointMake(bg.position.x + bg.size.width * 2, bg.position.y);
        }
        
    }];
}

@end

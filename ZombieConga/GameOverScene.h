//
//  GameOverScene.h
//  ZombieConga
//
//  Created by Zsolt Borsos on 12/11/2014.
//  Copyright (c) 2014 chester. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface GameOverScene : SKScene
-(id)initWithSize:(CGSize)size won:(BOOL)won;
@end

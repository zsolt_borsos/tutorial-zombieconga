//
//  MainMenuScene.m
//  ZombieConga
//
//  Created by Zsolt Borsos on 12/11/2014.
//  Copyright (c) 2014 chester. All rights reserved.
//

#import "MainMenuScene.h"
#import "MyScene.h"

@implementation MainMenuScene

-(id)initWithSize:(CGSize)size
{
    if (self = [super initWithSize:size]){
        SKSpriteNode *bg;
        bg = [SKSpriteNode spriteNodeWithImageNamed:@"MainMenu.png"];
        bg.position = CGPointMake(self.size.width/2, self.size.height/2);
        [self addChild:bg];
    }
    return self;
}


//touch handlers
- (void)touchesBegan:(NSSet *)touches
           withEvent:(UIEvent *)event
{
    SKAction *block = [SKAction runBlock:^{
        MyScene *myScene =
        [[MyScene alloc]initWithSize:self.size];
        SKTransition *reveal = [SKTransition doorwayWithDuration:0.5];
        [self.view presentScene:myScene transition:reveal];
    }];
    [self runAction:block];
   
}

- (void)touchesMoved:(NSSet *)touches
           withEvent:(UIEvent *)event
{
    //UITouch *touch = [touches anyObject];
    //CGPoint touchLocation = [touch locationInNode:self];
    
}

- (void)touchesEnded:(NSSet *)touches
           withEvent:(UIEvent *)event
{
    //UITouch *touch = [touches anyObject];
    //CGPoint touchLocation = [touch locationInNode:self];
    
    
}

@end

//
//  AppDelegate.h
//  ZombieConga
//
//  Created by student on 16/10/2014.
//  Copyright (c) 2014 chester. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
